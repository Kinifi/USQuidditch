﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	//the Axis of the joystick
	private float horAxis, vertAxis;

	//components
	private Rigidbody _rigid;

	//the last rotation passed from the joystick
	private Quaternion m_lastRotation;

	[Header("Player Config")]
	public float m_movementSpeed = 0.1f;

	// Use this for initialization
	void Start () {

		//get the components
		_rigid	= GetComponent<Rigidbody>();
	
	}
	
	// Update is called once per frame
	void Update () {

		horAxis = Input.GetAxis("Horizontal");
		vertAxis = Input.GetAxis("Vertical");

		//transform.eulerAngles = new Vector3( 0, Mathf.Atan2( vertAxis, horAxis) * -180 / Mathf.PI, 0 );
		float angle = Mathf.Atan2(horAxis, vertAxis) * Mathf.Rad2Deg; 
		transform.rotation = Quaternion.Euler(new Vector3(0, angle, 0));

		//check if the joystick is pushed forward
		if(horAxis > 0.5f || horAxis < -0.5f || vertAxis > 0.5f || vertAxis < -0.5f)
		{
			//save the last rotation
			m_lastRotation = transform.rotation;

			//move the player 
			transform.position += transform.forward * m_movementSpeed;
		}
		else
		{
			//rotate the player to the last rotation it used
			transform.rotation = m_lastRotation;

			//check if we should stop the player velocity
			if(_rigid.velocity != Vector3.zero)
			{
				//stop the player
				_rigid.velocity = Vector3.zero;
			}
		}


	}
}
