﻿using UnityEngine;
using System.Collections;

public class snitchController : MonoBehaviour {

	private Vector3 m_Goal;
	private NavMeshAgent m_Agent;

	// 0 = random
	// 1 = avoid seeker
	private int movementMode = 1;

	private GameObject[] m_seekers;


	void Start () {

		m_seekers = GameObject.FindGameObjectsWithTag("seeker");

		m_Agent = GetComponent<NavMeshAgent>();

		changeSnitchSpeed(20);

		generateNewPosition();

	}

	private void generateNewPosition()
	{
		//generate a random position for the snitch to move to
		m_Goal = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
		m_Agent.destination = m_Goal;
	}

	private void changeSnitchSpeed(float newSpeed)
	{
		//make snitch go faster
		m_Agent.speed = newSpeed;
	}

	// Update is called once per frame
	void Update () {
	
		//randomly generate a position to go to
		if(movementMode == 0)
		{
			if(m_Agent.remainingDistance < 0.5f)
			{
				//generate a new position
				generateNewPosition();
				movementMode = 1;
			}
		}
		else if(movementMode == 1)
		{
			//TODO: Bludger movement? 
			changeSnitchSpeed(50);
			m_Agent.SetDestination(m_seekers[0].transform.position);

		}


	}
}
